Soccer : To have list of teams and players of team. 

Steps to setup :   
	1. Checkout the project  
	2. Run the following commands  
	- composer install  
	- When asked for enter the database details (Or after composer installation is complete change the database realted settings in app/config/parameters.yml )
	- php bin/console cache:clear  
	- php bin/console doctrine:schema:drop --force  
	- php bin/console doctrine:schema:create  
	- php bin/console doctrine:migrations:migrate  
	- Give permissions for var folder to capture logs.  
  
Unit testing :  
	You can run the following command to check the unit testing ./vendor/bin/phpunit   
	Unit testing report can be found under /tmp/report  


