<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180701110521 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql', 
            'Migration can only be executed safely on mysql.'
        );

        $this->addSql("
            INSERT INTO `team` (`name`, `logo_uri`) VALUES ('Argentina', 'images/flags/ar.png');
            
            SET @teamid = LAST_INSERT_ID();

            INSERT INTO `player` (`first_name`, `last_name`, `image_uri`) 
            VALUES ('Lionel', 'Messi', 'images/players/ar-lm.jpeg');
            SET @playerid1 = LAST_INSERT_ID();

            INSERT INTO `player` (`first_name`, `last_name`, `image_uri`) 
            VALUES ('Paulo', 'Dybala', 'images/players/ar-pd.jpeg');
            SET @playerid2 = LAST_INSERT_ID();

            INSERT INTO `teams_players` (`team_id`, `player_id`) 
            VALUES (@teamid, @playerid1), (@teamid, @playerid2);
        ");

        $this->addSql("
            INSERT INTO `team` (`name`, `logo_uri`) VALUES ('France', 'images/flags/fr.png');
            
            SET @teamid = LAST_INSERT_ID();

            INSERT INTO `player` (`first_name`, `last_name`, `image_uri`) 
            VALUES ('Benjamin', 'Pavard', 'images/players/fr-bp.jpeg');
            SET @playerid1 = LAST_INSERT_ID();

            INSERT INTO `player` (`first_name`, `last_name`, `image_uri`) 
            VALUES ('Paul', 'Pogba', 'images/players/fr-pp.jpeg');
            SET @playerid2 = LAST_INSERT_ID();

            INSERT INTO `teams_players` (`team_id`, `player_id`) 
            VALUES (@teamid, @playerid1), (@teamid, @playerid2);
        ");   

        $this->addSql("
            INSERT INTO `team` (`name`, `logo_uri`) VALUES ('India', 'images/flags/in.png');
            
            SET @teamid = LAST_INSERT_ID();

            INSERT INTO `player` (`first_name`, `last_name`, `image_uri`) 
            VALUES ('Sunil', 'Chhetri', 'images/players/in-sc.jpeg');
            SET @playerid1 = LAST_INSERT_ID();

            INSERT INTO `player` (`first_name`, `last_name`, `image_uri`) 
            VALUES ('Jeje', 'Lalpekhlua', 'images/players/in-jl.jpeg');
            SET @playerid2 = LAST_INSERT_ID();

            INSERT INTO `teams_players` (`team_id`, `player_id`) 
            VALUES (@teamid, @playerid1), (@teamid, @playerid2);
        ");

        $this->addSql("
            INSERT INTO `team` (`name`, `logo_uri`) VALUES ('Netherlands', 'images/flags/nl.png');
            
            SET @teamid = LAST_INSERT_ID();

            INSERT INTO `player` (`first_name`, `last_name`, `image_uri`) 
            VALUES ('Arjen', 'Robben', 'images/players/nl-ar.jpeg');
            SET @playerid1 = LAST_INSERT_ID();

            INSERT INTO `player` (`first_name`, `last_name`, `image_uri`) 
            VALUES ('Robin van', 'Persie', 'images/players/nl-rvp.jpeg');
            SET @playerid2 = LAST_INSERT_ID();

            INSERT INTO `teams_players` (`team_id`, `player_id`) 
            VALUES (@teamid, @playerid1), (@teamid, @playerid2);
        ");     
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
         $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql', 
            'Migration can only be executed safely on mysql.'
        );

        $this->addSql('TRUNCATE TABLE teams_players');
        $this->addSql('TRUNCATE TABLE player');
        $this->addSql('TRUNCATE TABLE team');
    }
}
