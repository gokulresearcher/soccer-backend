<?php

namespace SportsAcademy\FifaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Default controller.
 *
 * @Route("api/v1")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/team/{teamid}", name="get_teams", methods={"GET"},
     * defaults={"teamid": "0"}, requirements={"teamid"="\d+"})
     */
    public function getTeamsAction($teamid)
    {
        $teams = $this->get('fifa.team_player_service')->getTeams($teamid);
            
        $data = array();
        foreach ($teams as $team) {            
            $data[] = array(   
                "id" => $team->getId(),          
                "name" => $team->getName() ,
                "logoUri" => $team->getLogoUri() 
            );
        }

        if ($teamid > 0 && count($data) > 0 && $team) {
            $data = array(
                'team' => $data[0]
            );
            foreach ($team->getPlayers() as $player) { 
                $data['players'][] = array(
                    "number" => $player->getId(),
                    "firstName" => $player->getFirstName(),
                    "lastName" => $player->getLastName(),
                    "imageUri" => $player->getImageUri() 
                );
            }
        }

        return $this->json($data);
    }
}
