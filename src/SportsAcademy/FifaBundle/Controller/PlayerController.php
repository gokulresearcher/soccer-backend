<?php

namespace SportsAcademy\FifaBundle\Controller;

use SportsAcademy\FifaBundle\Entity\Player;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use SportsAcademy\FifaBundle\Form\PlayerType;

/**
 * Player controller.
 *
 * @Route("player")
 */
class PlayerController extends Controller
{
    /**
     * Creates a new player entity.
     *
     * @Route("/new", name="player_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $player = new Player();
        $form = $this->createForm(PlayerType::class, $player);
        $form->handleRequest($request);
        
        $teamId = $request->get('id');
        $team = $this->get('fifa.team_player_service')->getTeams($teamId);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('fifa.team_player_service')->saveTeamPlayer($team[0], $player);
            return $this->redirectToRoute('team_show', array('id' => $team[0]->getId()));
        }

        return $this->render('@Fifa/player/new.html.twig', array(
            'player' => $player,
            'team' => $team[0],
            'form' => $form->createView(),
        ));
    }
}
