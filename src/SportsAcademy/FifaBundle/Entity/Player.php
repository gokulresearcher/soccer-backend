<?php

namespace SportsAcademy\FifaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Player
 *
 * @ORM\Table(name="player")
 * @ORM\Entity(repositoryClass="SportsAcademy\FifaBundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=50)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=50, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="image_uri", type="text", nullable=true)
     */
    private $imageUri;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Team", mappedBy="players", cascade={"persist"})
     */
    protected $teams;

    public function __construct()
    {
        $this->teams = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Player
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Player
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set imageUri
     *
     * @param string $imageUri
     *
     * @return Player
     */
    public function setImageUri($imageUri)
    {
        $this->imageUri = $imageUri;

        return $this;
    }

    /**
     * Get imageUri
     *
     * @return string
     */
    public function getImageUri()
    {
        return $this->imageUri;
    }

    /**
     * Get teams
     *
     * @return ArrayCollection|Team[]
     */
    public function getTeams()
    {
        return $this->teams;
    }
}

