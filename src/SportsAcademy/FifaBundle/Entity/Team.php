<?php

namespace SportsAcademy\FifaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="SportsAcademy\FifaBundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_uri", type="string", length=250, nullable=true)
     *
     * @Assert\File(
     *     maxSize = "2M",
     *     mimeTypes = {"image/jpeg", "image/png", "image/jpg"},
     *     maxSizeMessage = "The maxmimum allowed file size is 2MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    private $logoUri;

    /**
     * Many Team have Many Players.
     * @ORM\ManyToMany(targetEntity="Player", inversedBy="teams", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinTable(name="teams_players",
     *      joinColumns={@ORM\JoinColumn(name="team_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="player_id", referencedColumnName="id", onDelete="cascade", unique=true)}
     *      )
     */
    protected $players;

    function __construct() {
        $this->players = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set logoUri
     *
     * @param string $logoUri
     *
     * @return Team
     */
    public function setLogoUri(string $logoUri = null)
    {
        $this->logoUri = $logoUri;

        return $this;
    }

    /**
     * Get logoUri
     *
     * @return string
     */
    public function getLogoUri()
    {
        return $this->logoUri;
    }

    /**
     * Add player
     *
     * @param Player $player
     *
     * @return Team
     */
    public function addPlayer(Player $player)
    {
        if ($this->players->contains($player)) {
            return;
        }
        $this->players[] = $player;//you can use add method too
    }

    /**
     * Remove player
     *
     * @param Player $player
     *
     * @return Team
     */
    public function removePlayer(Player $player)
    {
        $this->players->removeElement($player);
        return $this;
    }
    /**
     * Get players
     *
     * @return ArrayCollection|Player[]
     */
    public function getPlayers()
    {
        return $this->players;
    }
}

