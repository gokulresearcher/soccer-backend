<?php

namespace SportsAcademy\FifaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use SportsAcademy\FifaBundle\Entity\Team;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class TeamType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');        
        $builder->add('logoUri', FileType::class, array(
            'label' => 'Upload Image (.png | .jpeg | .jpg)',
            'required'   => false,
            'data_class' => null,
        ));
        
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Team::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sportsacademy_fifabundle_team';
    }

}
