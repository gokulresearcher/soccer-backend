<?php

namespace SportsAcademy\FifaBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploadService {

    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload($subDirectory, UploadedFile $file = null)
    {
        if (!$file) {
            return;
        }

        $fileName = md5(uniqid()).'.'.$file->guessExtension();
         
        if (trim($subDirectory)) {
            $this->targetDirectory = $this->targetDirectory . DIRECTORY_SEPARATOR . $subDirectory;
        }
            
        $file->move($this->getTargetDirectory(), $fileName);

        return $subDirectory . DIRECTORY_SEPARATOR . $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
  
}