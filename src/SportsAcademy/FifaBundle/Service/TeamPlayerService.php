<?php

namespace SportsAcademy\FifaBundle\Service;

use SportsAcademy\FifaBundle\Entity\Team;
use SportsAcademy\FifaBundle\Entity\Player;
use Doctrine\ORM\EntityManager;

class TeamPlayerService {

    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getTeams(int $id = 0)
    {
        $teamRepository = $this->entityManager->getRepository(Team::class);        
        $condition = $id > 0 ? array("id" => $id) : array();
        $teams = $teamRepository->findBy($condition, array("id" => "DESC"));
        return $teams;
    }

    public function getPlayersBasedOnTeam(int $teamId = 0)
    {
        $teamRepository = $this->entityManager->getRepository(Team::class);  
        $team = $teamRepository->findBy(array("id" => $teamId), array("id" => "DESC"));
        if ($team) {
            return $team[0]->getPlayers();
        }
        return array();
    }

    public function removeTeam(Team $team)
    {
        $this->entityManager->remove($team);
        $this->entityManager->flush();
    }

    public function saveTeam(Team $team)
    {
        $this->entityManager->persist($team);
        $this->entityManager->flush();
    }

    public function saveTeamPlayer(Team $team, Player $player)
    {
        $team->addPlayer($player);
        $this->entityManager->persist($team);
        $this->entityManager->flush();
    }
}