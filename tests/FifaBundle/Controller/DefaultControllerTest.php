<?php

namespace Tests\FifaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testGetTeams()
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/team');
        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        //$this->assertJsonResponse($response, Response::HTTP_OK);
    }

    public function testGetTeamWithPlayer()
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/team/2');
        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
    }

}
