<?php

namespace Tests\FifaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TeamControllerTest extends WebTestCase
{
    public function testIndex()
    {        
        $client = static::createClient();
        $crawler = $client->request('GET', '/team/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testIndexTitlePresent()
    {        
        $client = static::createClient();
        $crawler = $client->request('GET', '/team/');
        $this->assertEquals(1, $crawler->filter('h4')->count());
    }

}
