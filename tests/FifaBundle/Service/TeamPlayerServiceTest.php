<?php

namespace Tests\FifaBundle\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use SportsAcademy\FifaBundle\Entity\Team;
use SportsAcademy\FifaBundle\Entity\Player;
use SportsAcademy\FifaBundle\Service\TeamPlayerService;
use SportsAcademy\FifaBundle\Repository\TeamRepository;

class TeamPlayerServiceTest extends WebTestCase
{
    protected $entityManager;

    protected function setUp()
    {
        $this->entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()->getMock();
    }

    public function testGetPlayersBasedOnTeam()
    {
        $teamPlayerService = new TeamPlayerService($this->entityManager);

        $team = $this->getMockBuilder('SportsAcademy\FifaBundle\Entity\Team')
        ->disableOriginalConstructor()->getMock();
        $team->expects($this->once())
            ->method('getPlayers')
            ->willReturn(Player::class);

        $teamRepository = $this->createMock(TeamRepository::class);
        $teamRepository->expects($this->once())
            ->method('findBy')
            ->withAnyParameters()
            ->willReturn([$team]);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->willReturn($teamRepository);

        $actualResult = $teamPlayerService->getPlayersBasedOnTeam(2);

        //Expected result would be instance of team
        $this->assertEquals(Player::class, $actualResult);
    }

    public function testGetPlayersBasedOnTeamInvalid()
    {
        $teamId = 0;
        $teamPlayerService = new TeamPlayerService($this->entityManager);

        $teamRepository = $this->createMock(TeamRepository::class);
        $teamRepository->expects($this->once())
            ->method('findBy')
            ->withAnyParameters()
            ->willReturn(null);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->willReturn($teamRepository);

        $actualResult = $teamPlayerService->getPlayersBasedOnTeam($teamId);

        //Expected result would be empty array as there is no team found with given id
        $this->assertEmpty($actualResult);
    }

    public function testRemoveTeam()
    {
        $team = $this->createMock(Team::class);

        $teamPlayerService = new TeamPlayerService($this->entityManager);
        $this->entityManager->expects($this->once())->method('remove')
            ->withAnyParameters($team);

        $teamPlayerService->removeTeam($team);
    }

    public function testSaveTeam()
    {
        $team = $this->createMock(Team::class);

        $teamPlayerService = new TeamPlayerService($this->entityManager);
        $this->entityManager->expects($this->once())->method('persist')
            ->withAnyParameters($team);

        $teamPlayerService->saveTeam($team);
    }

    public function testSaveTeamPlayer()
    {
        $team = $this->createMock(Team::class);
        $player = $this->createMock(Player::class);

        $teamPlayerService = new TeamPlayerService($this->entityManager);
        $this->entityManager->expects($this->once())->method('persist')
            ->withAnyParameters($team);

        $teamPlayerService->saveTeamPlayer($team, $player);
    }

}
