$(document).ready(function(){

    var url = window.location.pathname;
    var teamid = url.substring(url.lastIndexOf('/') + 1);

    showLoading();

    var service = '/api/v1';
    $.ajax({
        type: "GET",
        url: service + '/team/' + teamid,
        dataType: "json",
        cache: false,
        success: function (data) {
            populateList(data, 'handlebars-player-list');
            hideLoading();       
        },        
        error: function (msg) {
            var errMsg = 'Could not load player list please contact administrator.';
            showErrorMessage(errMsg);  
            hideLoading();     
        }
    });
});

