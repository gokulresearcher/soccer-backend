$(document).ready(function(){

    showLoading();

    var service = '/api/v1';
    $.ajax({
        type: "GET",
        url: service + '/team',
        dataType: "json",
        cache: false,
        success: function (data) {
            data['teams'] = data;
            populateList(data, 'handlebars-team-list');
            hideLoading();       
        },        
        error: function (msg) {
            var errMsg = 'Could not load team list please contact administrator.';
            showErrorMessage(errMsg);
            hideLoading();     
        }
    });
});

